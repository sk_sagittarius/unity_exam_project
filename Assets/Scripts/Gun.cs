﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gun : MonoBehaviour
{
    [SerializeField]
    private float maxAmmo, damage, range, shootSpeed, clip; // патроны, урон, дальность, скорострельность, обойма
    private float currentAmmo; // текущее количество патронов

    [SerializeField]
    private ParticleSystem shootEffect; // эффект выстрела

    [SerializeField]
    private AudioSource audioSource; // звук

    [SerializeField]
    private GameObject hitEffect; // эффект попадания

    [SerializeField]
    private Camera playerCamera;

    [SerializeField]
    private Text ammoCount, warningMessage, clipCount; // количество патронов, уведомление, количество в обойме

    private Enemy enemy;



    void Start()
    {
        currentAmmo = maxAmmo;
    }

    private void Update()
    {
        if(Input.GetButtonDown("Fire1")) // left mouse button
        {
            Shoot(); // выстрел
        }

        // перезарядка
        if(currentAmmo <= 0)
        {
            warningMessage.gameObject.SetActive(true);
            warningMessage.text = "Для перезарядки нажмите R";
        }
        if(Input.GetKey("r"))
        {
            warningMessage.gameObject.SetActive(false);
            if(clip > 0) // TODO разобраться почему прибавляет сразу 30, а не по 10. потому что clip больше 0 и всегда заходит? а инпут?
            {
                currentAmmo += 10;
                clip -= 10;
                Debug.Log("from clip");
            }
            if(clip <= 0 && currentAmmo <= 0)
            {
                warningMessage.gameObject.SetActive(true);
                warningMessage.text = "Нет патронов";
            }
        }

        ammoCount.text = "Патроны: " + currentAmmo.ToString();
        clipCount.text = "Обойма: " + clip.ToString();
    }

    private void Shoot()
    {
        RaycastHit hit;
        if(currentAmmo>0)
        {
            if(Physics.Raycast(playerCamera.transform.position, playerCamera.transform.forward, out hit, range)) // откуда, куда, сохраняем, дальность
            {
                shootEffect.Play(); // проигрывание эффекта
                GameObject effect = Instantiate(hitEffect, hit.point, Quaternion.LookRotation(hit.normal)); // создаю в реальном времени (что, где, как повернуть) 
                Destroy(effect, 1f); // уничтожить эффект через 1 секунду
                Debug.Log("Hit name " + hit.collider.name);
                if(hit.collider.tag == "Enemy") // попадание в объекты
                { // TODO у каждого врага должна быть собственная жизнь
                    // нанести урон
                    Enemy enemy = hit.collider.GetComponent<Enemy>();
                    enemy.GetDamage(damage);
                    
                }
            }
            currentAmmo--; // минус патрон
        }
    }

    public void GetBox()
    {
        clip += 10;
    }


}
