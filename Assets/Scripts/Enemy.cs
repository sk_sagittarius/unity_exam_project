﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private float enemyLife;

    public void GetDamage(float damage)
    {
        if(enemyLife >= 0)
        {
            enemyLife -= damage * Time.deltaTime;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
