﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private Gun gun;
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) 
    {
        Debug.Log("Triggered " + other.tag);
        if (other.tag == "Box") // ящик
        {       
            gun.GetBox(); // нахожу обойму
            Destroy(other.gameObject); // уничтожение другого объекта
        }
    }
}
